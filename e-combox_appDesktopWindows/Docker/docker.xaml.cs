﻿using System;
using System.ServiceProcess;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using e_combox_appDesktopWindows.Scripts;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Media;
using System.Windows.Threading;
using System.Management;

namespace e_combox_appDesktopWindows.D_ocker
{
    /// <summary>
    /// Logique d'interaction pour docker.xaml
    /// Interraction avec les paramètres docker 
    /// </summary>
    public partial class docker : UserControl
    {

        static DispatcherTimer timer = new DispatcherTimer
        {
            Interval = TimeSpan.FromMilliseconds(2500),
            IsEnabled = false
        };
        readonly string scriptsDirectory = string.Format(@"..\..\Scripts\");
        readonly string imagesDirectory = string.Format(@"..\..\Images\");
        

        public docker()
        {
            InitializeComponent();
        }

        //Activer ou désactiver docker

        public void launch()
        {
            ServiceController sc = new ServiceController("com.docker.service");
            Console.WriteLine("Status" + sc.Status);

            if (Process.GetProcessesByName("Docker Desktop").Length > 0)
            {
                Console.WriteLine("Docker est ici allumé");
                dockerStatus.Content = "Docker est correctement démarré";
                this.img_allume.Source = new BitmapImage(new Uri(imagesDirectory + "success.png", UriKind.Relative));
            }
            else
            {
                Console.WriteLine("Docker est ici éteint");
                dockerStatus.Content = "Docker est éteint";
                this.img_allume.Source = new BitmapImage(new Uri(imagesDirectory + "cancel.png", UriKind.Relative));
            }
            //TODO déplacer dans une méthode qui prend le statut en paramètre


            if (timer.IsEnabled == false)
            {
                timer.IsEnabled = true;
                timer.Tick += new EventHandler(Dispatcher_timer);

            }
        }

        private void Dispatcher_timer(object sender, EventArgs e)
        {
            //récupére la mémoire RAM totale du pc
            var capacity = new ManagementObjectSearcher("SELECT Capacity FROM Win32_PhysicalMemory").Get().Cast<ManagementObject>().Sum(x => Convert.ToInt64(x.Properties["Capacity"].Value));
 
            //recupére la mémoire utilisé par docker
            Process[] process = Process.GetProcessesByName("Docker Desktop");
            if (process.Length > 0)
            { 
                long memoireAloue = 0;
                foreach (Process pro in process)
                {
                    memoireAloue += pro.PrivateMemorySize64;
                }
                // convertie en mo
                double memoireAloueMega = (long)Convert.ToDouble(memoireAloue / Math.Pow(1024, 2));
                double capacityMega = (long)Convert.ToDouble(capacity / Math.Pow(1024, 2));

                // calcule pourcentage memoire utilisé
                double pourcentageMemoire = (memoireAloue * 100) / capacity;
                                          
                if (pourcentageMemoire < 1)
                {
                    memoryValue.Content = "Mémoire utilisée < 1 %  Mémoire totale : " + ToFileSize(capacity);
                }

                else
                {          
                    memoryValue.Content = "Mémoire utilisé à " + pourcentageMemoire + " %   Mémoire totale : " + ToFileSize(capacity);
                }
                this.StockageSize();
                this.memoryBar.Value = pourcentageMemoire;
                this.memoryBar.Minimum = 0;
                this.memoryBar.Maximum = 100;
                

                // Affichage d'un bouton permettant l'optimisation de la ram en cas ou celle-ci dépasse les 80% d'utilisation à completer
                if (pourcentageMemoire > 80)
                {
                    btn_optimise.Visibility = Visibility.Visible;

                }
                else
                {
                    btn_optimise.Visibility = Visibility.Hidden;
                }
               
                // détermine la couleur de la barre de progression
                Color green = Color.FromRgb(84, 171, 138);
                Color orange = Color.FromRgb(221, 121, 2);
                Color red = Color.FromRgb(208, 89, 98);

                if (pourcentageMemoire < 34)
                {
                    memoryBar.Foreground = new SolidColorBrush(green);
                }
                else if (pourcentageMemoire >33 && pourcentageMemoire < 67)
                {
                    memoryBar.Foreground = new SolidColorBrush(orange);
                }
                else
                {
                    memoryBar.Foreground = new SolidColorBrush(red);
                }
            }
        }

        public List<double> DockerSizeTotalUse()
        {
            DriveInfo[] allDrives = DriveInfo.GetDrives();

            List<double> liste_stockage_total = new List<double>();

            foreach (DriveInfo c in allDrives)
            {
                if (c.IsReady == true)
                {
                    liste_stockage_total.Add(c.TotalFreeSpace);
                    liste_stockage_total.Add(c.TotalSize);
                }
            }
            return (liste_stockage_total);
        }
        public void StopTimer()
        {
            timer.Tick -= Dispatcher_timer;
            timer.Stop();
        }

        public async void StockageSize()
        {
            PowerShellExecution pse1 = new PowerShellExecution();
            // Affichage version Docker
            string version = await pse1.ExecuteShellScript(scriptsDirectory + "version_docker.ps1");

            //-------------
            try
            {
                PowerShellExecution pse = new PowerShellExecution();

                // Instanciation d'un nouvel objet 

                string size = await pse.ExecuteShellScript(scriptsDirectory + "stockage.ps1");
                string value = size;
                //------------

                // delimiter va permettre de séparer tous les élements à partir d'un espace ici
                char delimiter = ' ';
                //------------

                string chaineTest = "";

                for (int i = 1; i < value.Length; i++)
                {
                    bool teste = true;

                    if (value[i] == ' ' && value[i - 1] == ' ')
                    {
                        teste = false;

                    }
                    if (teste == true)
                    {
                        chaineTest += value[i - 1];
                    }

                }


                //Créer une liste automatiquement en mettant à la ligne entre chaque espace
                string[] substrings = chaineTest.Split(delimiter);
                // On remplace les . par des , pour permettre le calcul
                substrings[8] = substrings[8].Replace(".", ",");
                substrings[14] = substrings[14].Replace(".", ",");
                substrings[21] = substrings[21].Replace(".", ",");
                string[] liste = { substrings[8], substrings[14], substrings[21] };
                //-------------

                // Création d'une liste contenant un double
                List<double> liste_2 = new List<double>();
                double new_result = 0;
                //-------------

                foreach (string element in liste)
                {

                    try
                    {

                        if (element.Length <= 1)
                        {
                            int.Parse(element.Substring(0, 1));
                        }
                        else
                        {
                            int.Parse(element.Substring(element.Length - 2, 1));
                            liste_2.Add(double.Parse(element.Substring(0, element.Length - 1)));
                        }



                    }
                    catch (Exception)
                    {
                        if (element.Substring(element.Length - 2, 1) == "G")
                        {
                            liste_2.Add((double.Parse(element.Substring(0, element.Length - 2))) * Math.Pow(2, 30));
                        }
                        if (element.Substring(element.Length - 2, 1) == "M")
                        {
                            liste_2.Add((double.Parse(element.Substring(0, element.Length - 2))) * Math.Pow(2, 20));
                        }
                        if (element.Substring(element.Length - 2, 1) == "k")
                        {
                            liste_2.Add((double.Parse(element.Substring(0, element.Length - 2))) * Math.Pow(2, 10));
                        }
                    }
                }
                foreach (double i in liste_2)
                {

                    new_result += i;
                }
                List<double> DockerSizeTotaluse = DockerSizeTotalUse();
                double resultat_total = DockerSizeTotaluse[1];
                double EspaceDispo = DockerSizeTotaluse[0];
                /*foreach (double i in DockerSizeTotaluse)
                {
                    resultat_total += i;
                }*/

                double pourcentage = Math.Round((new_result / resultat_total) * 100);

                if (pourcentage == 0)
                {
                    sizeLabel.Content = "Espace disque utilisé par Docker : " + this.ToFileSize((double)new_result) + " (<1%)          Espace disque disponible : " + this.ToFileSize((double)EspaceDispo) +"/" + this.ToFileSize((double)resultat_total);
                }
                else
                {
                    sizeLabel.Content = "Espace disque utilisé par Docker : " + this.ToFileSize((double)new_result) + " (" + Math.Round((new_result / resultat_total) * 100) + "%)" + "          Espace disque disponible : " + this.ToFileSize((double)EspaceDispo) + "/" + this.ToFileSize((double)resultat_total);
                }


                this.Size.Value = new_result;

                this.Size.Maximum = resultat_total;


                //Changement de couleur de la progressbar de stockage utilisé
                Color green = Color.FromRgb(84, 171, 138);
                Color orange = Color.FromRgb(221, 121, 2);
                Color red = Color.FromRgb(208, 89, 98);

                if (pourcentage < 50)
                {
                    Size.Foreground = new SolidColorBrush(green);
                }
                else if (pourcentage >= 50 && pourcentage <= 79)
                {
                    Size.Foreground = new SolidColorBrush(orange);
                }
                else
                {
                    Size.Foreground = new SolidColorBrush(red);
                }







            }
            catch (Exception)
            {
                PowerShellExecution pse = new PowerShellExecution();
                await pse.ExecuteShellScript(scriptsDirectory + "checkEcomBoxStatus.ps1");
            }


            if (version.Length > 15)
            {
                int dock_limit = version.Length;
                versiondocker.Content = "Version de Docker : " + version.Substring(15, dock_limit - 31);
            }

        }
        public string ToFileSize(double value)
        {
            string[] suffixes = { "bytes", "KB", "MB", "GB",
        "TB", "PB", "EB", "ZB", "YB"};
            for (int i = 0; i < suffixes.Length; i++)
            {
                if (value <= (Math.Pow(1024, i + 1)))
                {
                    double highCalc = value / Math.Pow(1024, i);
                    return highCalc.ToString("0.00") + " " + suffixes[i];
                }
            }
            double calc = value / Math.Pow(1024, suffixes.Length - 1);
            return calc.ToString("0.00") + " " + suffixes[suffixes.Length - 1];
        }

        private void btn_modifmemory_Click(object sender, RoutedEventArgs e)
        {
            Docker.ModificateurMemory window = new Docker.ModificateurMemory();
            window.Show();
            
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OptimizeDisk();
            //Process.Start("Powershell.exe -File C:/Users/btssio1/ecombox/e-comBox_appDesktopWindows/e-combox_appDesktopWindows/Scripts/reduireVHDX.ps1");
            MessageBox.Show("Votre disque est optimiser");
        }
        private async void OptimizeDisk()
        {
            PowerShellExecution ps = new PowerShellExecution();
            string mechant =  await ps.ExecuteShellScript(scriptsDirectory + "reduireVHDX.ps1");
            
        }

    }
}
