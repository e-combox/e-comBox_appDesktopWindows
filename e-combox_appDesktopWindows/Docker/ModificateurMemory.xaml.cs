﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.IO;
using System.Text.RegularExpressions;
using System;
using System.Management;

namespace e_combox_appDesktopWindows.Docker
{
    /// <summary>
    /// Logique d'interaction pour ModificateurMemory.xaml
    /// </summary>
    public partial class ModificateurMemory : Window
    {
        public int ramSystem = 0;
        public ModificateurMemory()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            string env = Environment.GetEnvironmentVariable("USERPROFILE");
            string ternary = File.Exists($"{env}/.wslconfig") ? "wslconfig" : "e-comBox_setupWin10pro/.wslconfig";
            using (StreamReader sr = new StreamReader($"{env}/{ternary}"))
            {
                string line = "";

                while ((line = sr.ReadLine()) != null)
                {
                    Console.WriteLine(line);
                    // s'arret à la ligne concernant la ram
                    if (line.Contains("#swap"))
                    {
                        this.txtNbMemory.Text = line.Split('=')[1];
                    }
                }
            }

        }

        private void ButtonValidMemory(object sender, RoutedEventArgs e)
        {
            //récupére la mémoire RAM totale du pc
            long ramTotal = new ManagementObjectSearcher("SELECT Capacity FROM Win32_PhysicalMemory").Get().Cast<ManagementObject>().Sum(x => Convert.ToInt64(x.Properties["Capacity"].Value));
         
            string input = this.txtNbMemory.Text;
            // test si l'utilisateur a entrer dans la texte box uniquement des chiffres
            Regex pattern = new Regex("(?:[0-9]{1,3})(?:[GM][B])");
            if (pattern.IsMatch(input))
            {
                // verifie que la ram à alloué n'exede pas 70% de la ram total du pc
                if (Int32.Parse(input.Substring(0, input.Length - 2)) < ramTotal * 0.7)
                {
                    //trouve et lie le fichier wslconfig
                    string env = Environment.GetEnvironmentVariable("USERPROFILE");
                    string ternary = File.Exists($"{env}/.wslconfig") ? "wslconfig" : "e-comBox_setupWin10pro/.wslconfig";     
                    string[] lines = File.ReadAllLines($"{env}/{ternary}");

                    for (int i = 0; i < lines.Length; i++)
                    {
                        // s'arret à la ligne concernant la ram
                        if (lines[i].Contains("#swap"))
                            lines[i] = lines[i].Split('=')[0] + "=" + input;
                    }
                    File.WriteAllLines($"{env}/{ternary}", lines);
                    Close();
                }                
                else
                {
                    MessageBox.Show("La valeur saisie ne doit pas exeder 70% de la ram total" + "ram total : " + ramTotal);
                }
            }
            else
            {
                MessageBox.Show("Erreur veuillez insérer des chiffres suivi de GB ou MB");
            }
        }


    }
}

