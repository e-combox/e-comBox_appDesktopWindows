﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using e_combox_appDesktopWindows.Scripts;
using MaterialDesignThemes.Wpf;
using System.IO;

namespace e_combox_appDesktopWindows.e_comBox
{
    /// <summary>
    /// Logique d'interaction pour EcomBox.xaml
    /// </summary>
    public partial class
        EcomBox : UserControl
    {
        string scriptsDirectory = string.Format(@"..\..\Scripts\");
        string imagesDirectory = string.Format(@"..\..\Images\");
        bool ecomboxIsStarted = false;

        public EcomBox()
        {
            InitializeComponent();

        }

        private void Button_Start_Click(object sender, RoutedEventArgs e)
        {

            ServiceController sc = new ServiceController("com.docker.service");

            if (this.ecomboxIsStarted && (sc != null || sc.Status == ServiceControllerStatus.Running || sc.Status == ServiceControllerStatus.StartPending))
            {
                
                this.stopEcomBox();
            }
            else
            {
                this.startEcomBox();
            }

        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            this.checkStatus();
        }
        public void showPanel(string text)
        {
            myPanel.Visibility = Visibility.Visible;
            bandeau.Text = text;
        }
        
        private async void startEcomBox() //Demarre e-comBox
        {
            showPanel("Démarrage en cours");
            bool pasDemarre = false;
            //Vérification de l'état de Docker
            PowerShellExecution pse = new PowerShellExecution();
            string status = await pse.ExecuteShellScript(scriptsDirectory + "checkEcomboxStatus.ps1");
            //Si Docker eteint alors l'allumer
            if (status.Contains("Stopped"))
            {
                ProcessStartInfo myInfo = new ProcessStartInfo();
                myInfo.FileName = "Docker Desktop.exe";
                myInfo.WorkingDirectory = "C:/Program Files/Docker/Docker";
                Process.Start(myInfo);
                pasDemarre = true;
            }

            //Boucle pour attendre le lancement complet de Docker avant de lancer l'url
            while (pasDemarre)
            {
                string estDemarre = await pse.ExecuteShellScript(scriptsDirectory + "checkEcomboxStatus.ps1");
                if (estDemarre.Contains("Started"))
                {
                    pasDemarre = false;
                }
                else
                {
                    Console.WriteLine("Heho");
                    await pse.ExecuteShellScript(scriptsDirectory + "timer.ps1");
                }
            }

            await pse.ExecuteShellScript(scriptsDirectory + "lanceURL.ps1");
            
            this.checkStatus();

        }

        private async void stopEcomBox() //pour arreter ecomBox
        {
            PowerShellExecution pse = new PowerShellExecution();
            showPanel("Arrêt en cours");
            string result = await pse.ExecuteShellScript(scriptsDirectory + "stopDocker.ps1");
            Console.WriteLine("Arrêt de docker  : " + result);
            
            this.checkStatus();
        }

        private async void checkStatus() //s'occupe des bouttons pour démarrer ecomBox
        {
            PowerShellExecution pse = new PowerShellExecution();
            string status = await pse.ExecuteShellScript(scriptsDirectory + "checkEcomboxStatus.ps1");

            if (status.Contains("Stopped"))
            {
                this.Button_Go_On_Website.Visibility = Visibility.Collapsed;
                this.imgStart.Source = new BitmapImage(new Uri(imagesDirectory + "power-off.png", UriKind.Relative));
                this.txtStart.Text = "Démarrer e-comBox";
                this.ecomboxIsStarted = false;

            }
            else
            {
                this.Button_Go_On_Website.Visibility = Visibility.Visible;
                this.imgStart.Source = new BitmapImage(new Uri(imagesDirectory + "power.png", UriKind.Relative));
                this.txtStart.Text = "Stopper e-comBox";
                this.ecomboxIsStarted = true;

            }
            myPanel.Visibility = Visibility.Hidden;

        }

        private void Button_Renew_Click(object sender, RoutedEventArgs e)
        {
            this.RenewEnv();
        }
        private async void RenewEnv()
        {
            showPanel("Reinitialisation de l'environnement");
            PowerShellExecution pse = new PowerShellExecution();
            await pse.ExecuteShellScript(scriptsDirectory + "restartApplication.ps1");
            this.checkStatus();
        }

        private async void DialogHost_DialogOpened(object sender, DialogOpenedEventArgs eventArgs) //permet d'envoyer un message quand le proxy n'est pas encore configuré 
        {
            PowerShellExecution pse = new PowerShellExecution();
            string result = await pse.ExecuteShellScript(scriptsDirectory + "test.ps1");
            string message;

            if (result.Length > 0)
            {
                message = "Vous disposez d'un proxy, veuillez configurer l'adresse ci-dessous dans les paramètres de Docker";
            }
            else
            {
                message = "Vous ne disposez pas de proxy, veuillez modifier ce paramètre dans Docker, s'il était activé auparavant";
            }
            var person = new Proxy
            {
                Address = result,
                Message = message
            };

            this.dialogProgress.IsOpen = false;

            await this.dialogInfo.ShowDialog(person);
        }

        private void Button_Uninstall_Websites_Click(object sender, RoutedEventArgs e)

        {
            UninstallWebsites();
        }
        private async void UninstallWebsites() //permet de desinstaller tout les sites web à partir de l'application de bureau
        {
            PowerShellExecution deletingSites = new PowerShellExecution();
            showPanel("Désinstallation des sites");
            string status = await deletingSites.ExecuteShellScript(scriptsDirectory + "supprimeSites.ps1");
            Console.WriteLine(status);
            if (status.Contains("Completed"))
            {
                MessageBox.Show("Désinstallation bien complétée", "Message d'acquisition");
            }
            else
            {
                MessageBox.Show("Désinstallation mal exécutée", "Erreur désinstallation", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            this.checkStatus();
        }

        private void Button_Go_On_Website_Click(object sender, RoutedEventArgs e)
        {

            Process.Start("http://localhost:8888/#/ecombox/dashboard");
        }

        private void Button_Auth_Click(object sender, RoutedEventArgs e)
        {
            Authentification();
        }

        private async void Authentification()
        {
            PowerShellExecution auth = new PowerShellExecution();
            string status = await auth.ExecuteShellScript(scriptsDirectory + "creerAuthApplication.ps1");

        }
    }

}
