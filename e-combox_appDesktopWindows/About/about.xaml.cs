﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace e_combox_appDesktopWindows.A_bout
{
    /// <summary>
    /// Logique d'interaction pour docker.xaml
    /// </summary>
    public partial class about : UserControl
    {
        public about()
        {
            InitializeComponent();
            getVersion();
        }

        //Photo de la licence
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("https://cecill.info");
        }

        //Logo du lycée Laetitia
        private void laetitia_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://llb.ac-corse.fr/");
        }

        //Photo de réseau Certea
        private void certa_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Process.Start("https://www.reseaucerta.org/");
        }

        private void getVersion()
        {
            string version = "";
            string env = Environment.GetEnvironmentVariable("USERPROFILE");
            if (File.Exists($"{env}/version.txt"))
            {


                using (StreamReader sr = File.OpenText($"{env}/version.txt"))
                {
                    string line = "";

                    while ((line = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                        // s'arret à la ligne concernant la ram
                        if (line.Contains("version"))
                        {
                            
                            string a = line.Split('=')[1];
                            version = a;
                            Console.WriteLine("if" + version);



                        }

                    }
                }
            }
            this.version.Text = version;
        }
       
    }
}
