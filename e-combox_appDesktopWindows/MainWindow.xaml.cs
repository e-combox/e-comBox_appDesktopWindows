﻿using e_combox_appDesktopWindows.Scripts;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using MessageBox = System.Windows.Forms.MessageBox;
using Newtonsoft.Json;

namespace e_combox_appDesktopWindows
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        string scriptsDirectory = string.Format(@"..\..\Scripts\");
        Uri google = new Uri("http://google.fr");
        private D_ocker.docker dock;
        public MainWindow()
        {

            InitializeComponent();
            verifIp();
            checkVersion();

        }

        private void checkVersion()
        {
            Console.WriteLine("test1");
            string env = Environment.GetEnvironmentVariable("USERPROFILE");


            if (File.Exists($"{env}/informations.json") && File.Exists($"{env}/version.txt"))
            {
                Console.WriteLine("test2");

                string lastVersion = "";
                string version = "";
                using (StreamReader file = File.OpenText($"{env}/informations.json"))
                {
                    string line = "";

                    while ((line = file.ReadLine()) != null)
                    {
                        Console.WriteLine("line :  " + line );
                        // s'arret à la ligne concernant la ram
                        if (line.Contains("version"))
                        {
                            Console.Write("test3");
                            version = line.Split(':')[1];
                        
                        }

                    }
                   /* string json = file.ReadToEnd();
                    Version versionJson = JsonConvert.DeserializeObject<Version>(File.ReadAllText($"{env}/informations.json"));
                    lastVersion = versionJson.ToString()   */               
                 
;                }
               
                Console.WriteLine(lastVersion + "vite " + "test5 :" + version);
                using (StreamReader sr = File.OpenText($"{env}/version.txt"))
                {
                    string line = "";

                    while ((line = sr.ReadLine()) != null)
                    {
                        Console.WriteLine(line);
                        // s'arret à la ligne concernant la ram
                        if (line.Contains("version"))
                        {
                            string a = line.Split('=')[1];
                            version = a;
                            
                            
                        }

                    }
                }
                Console.WriteLine(version, "versionnnnnn") ;
                
                if(version != lastVersion)
                {
                    MessageBox.Show("une nouvelle version de l'application exists version : lastVersion.versions. Pour obtenir une aide dans ca la mise a jours consulté LastVersion.helpUpdate.");
                }
            }
            else
            {
                using (WebClient wc = new WebClient())
                {
                    wc.Headers.Add("a", "a");
                    try
                    {
                        wc.DownloadFile("https://gitlab.com/e-combox/e-combox_update/-/raw/master/informations.json", $"{env}/informations.json");
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString() + " label");
                    }
                }
            }
  
        }

        private string getActiveIp()
        {
         
            foreach (var netI in NetworkInterface.GetAllNetworkInterfaces())
            {
                if (netI.NetworkInterfaceType != NetworkInterfaceType.Wireless80211 && (netI.NetworkInterfaceType != NetworkInterfaceType.Ethernet || netI.OperationalStatus != OperationalStatus.Up)) continue;
                foreach (var uniIpAddrInfo in netI.GetIPProperties().UnicastAddresses.Where(x => netI.GetIPProperties().GatewayAddresses.Count > 0))
                {

                    if (uniIpAddrInfo.Address.AddressFamily == AddressFamily.InterNetwork)
                        return uniIpAddrInfo.Address.ToString();
                }
            }
            return "You local IPv4 address couldn't be found...";
        }

        public string CheckDefaultProxyForRequest(Uri resource)
        {

            WebProxy proxy = WebProxy.GetDefaultProxy();

            // See what proxy is used for resource.
            Uri resourceProxy = proxy.GetProxy(resource);

            // Test to see whether a proxy was selected.
            if (resourceProxy == resource)
            {
                string resProxy = resource.ToString().Substring(7);
                return resProxy.Substring(0, resProxy.Length - 1); ;
            }
            else
            {
                string resProxy = resourceProxy.ToString().Substring(7);
                return resProxy.Substring(0, resProxy.Length - 1); ;
            }
        }

        private List<string> getDockerConf()
        {
            string line;
            List<string> paramRes = new List<string>();
            string env = Environment.GetEnvironmentVariable("USERPROFILE");

            StreamReader sr = new StreamReader($"{env}/.docker/confEcombox/param.conf");
            //Read the first line of text
            line = sr.ReadLine();
            //Continue to read until you reach end of file
            while (line != null)
            {
                //write the lie to console window
                Console.WriteLine(line);
                paramRes.Add(line);

                //Read the next line
                line = sr.ReadLine();

            }
            //close the file
            sr.Close();
            Console.ReadLine();
            return paramRes;
        }

        private List<string> getParamConf()
        {
            string line;
            List<string> paramRes = new List<string>();
            string env = Environment.GetEnvironmentVariable("USERPROFILE");

            StreamReader sr = new StreamReader($"{env}/.docker/confEcombox/param.conf");
            //Read the first line of text
            line = sr.ReadLine();
            //Continue to read until you reach end of file
            while (line != null)
            {
                //write the lie to console window
                Console.WriteLine(line);
                paramRes.Add(line);

                //Read the next line
                line = sr.ReadLine();

            }
            //close the file
            sr.Close();
            Console.ReadLine();
            return paramRes;
        }

        private string getIpParamConf()
        {
            List<string> param = getParamConf();
            return param[0].Substring(11);
        }

        private string getProxyParamConf()
        {

            List<string> param = getDockerConf();
            if (String.IsNullOrEmpty(param[4].Split('=')[1]) == false)
            {
                return param[4].Split('=')[1];
            }
            return null;
        }

        private async void verifIp()
        {
            string ipActive = getActiveIp();
            Console.WriteLine(ipActive);
            string ipParamConf = getIpParamConf();
            Console.WriteLine(ipParamConf);
            string proxyParamConf = getProxyParamConf();
            Console.WriteLine(proxyParamConf + " Param.conf");
            string proxy = CheckDefaultProxyForRequest(google);
            Console.WriteLine(proxy + " proxy");
            if (ipParamConf != null)
            {
                Console.WriteLine(proxy + "hja");
                if (ipActive == ipParamConf && proxy == proxyParamConf)
                {
                    Console.WriteLine("Les ip et le proxy correspondent");
                }
                else
                {
                    bool pasDemarre = true;
                    Console.WriteLine("Les ip et le proxy ne correspondent pas lancement de la reconfiguration");
                    PowerShellExecution pse = new PowerShellExecution();
                    await pse.ExecuteShellScript(scriptsDirectory + "restartApplication.ps1");
                    string status = await pse.ExecuteShellScript(scriptsDirectory + "checkEcomboxStatus.ps1");
                    if (status.Contains("Stopped"))
                    {
                        ProcessStartInfo myInfo = new ProcessStartInfo();
                        myInfo.FileName = "Docker Desktop.exe";
                        myInfo.WorkingDirectory = "C:/Program Files/Docker/Docker";
                        Process.Start(myInfo);
                        pasDemarre = true;
                    }
                    while (pasDemarre)
                    {
                        string estDemarre = await pse.ExecuteShellScript(scriptsDirectory + "checkEcomboxStatus.ps1");
                        if (estDemarre.Contains("Started"))
                        {
                            pasDemarre = false;
                        }
                        else
                        {
                            Console.WriteLine("Heho");
                            await pse.ExecuteShellScript(scriptsDirectory + "timer.ps1");
                        }
                    }
                }
            }
        }


        private void TabablzControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            string item = ((TabItem)((Dragablz.TabablzControl)sender).SelectedItem).Uid;
            if (item == "2")
            {
                dock = (D_ocker.docker)((Dragablz.TabablzControl)sender).SelectedContent;
                dock.launch();
            }
            else
            {
                if (dock != null)
                {
                    dock.StopTimer();
                    dock = null;
                }

            }

        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (MessageBox.Show("Voulez-vous vraiment fermer l'application ?", "Exit", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
