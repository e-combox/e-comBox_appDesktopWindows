﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace e_combox_appDesktopWindows.Settings
{
    
    /// <summary>
    /// Logique d'interaction pour Settings.xaml
    /// </summary>
    public partial class Settings : UserControl
    {
        public Settings()
        {
            InitializeComponent();
            addConfiguration();
        }
        private void addConfiguration()
        { 
            string user = Environment.GetEnvironmentVariable("USERPROFILE");
            string chemin = string.Format(@"{0}/.docker/confEcombox/param.conf", user);
            Console.WriteLine(chemin);
            using (StreamReader reader = new StreamReader(chemin))
            {
                string line;
                List<string> listLines = new List<string>();
                while((line = reader.ReadLine()) != null)
                {
                    listLines.Add(line);
                }
                //Affichage des paramètres déjà enregistré
                this.txtAdresseIPecombox.Text = listLines[0].Split('=')[1];
                this.txtPortECombox.Text = listLines[1].Split('=')[1];
                this.txtPortPortainer.Text= listLines[2].Split('=')[1];
                this.txtPortSites.Text=listLines[3].Split('=')[1];
                string socket = listLines[4].Split('=')[1];
                if (!string.IsNullOrEmpty(socket))
                {
                    Console.WriteLine(socket + "pas null?");
                    btnActiveProxy.IsChecked = true;
                    this.txtPortProxy.Text = socket.Split(':')[1];
                    this.txtipAdresse.Text = socket.Split(':')[0];
                    this.txtProxyExceptions.Text = listLines[5].Split('=')[1];
                }
                VerifButon();
            }
        }
        //Vérifier si Proxy est marqué comme activé ou non
        private void VerifButon()
        {
            
            if (btnActiveProxy.IsChecked == false)
            {
                this.txtPortProxy.Text = "";
                this.txtipAdresse.Text = "";
                this.txtProxyExceptions.Text = "";
                this.txtPortProxy.IsEnabled = false;
                this.txtipAdresse.IsEnabled = false;
                this.txtProxyExceptions.IsEnabled = false;
            }
            else
            {
                
                this.txtPortProxy.IsEnabled = true;
                this.txtipAdresse.IsEnabled = true;
                this.txtProxyExceptions.IsEnabled = true;
            }
        }

        private void btnValider_Click(object sender, RoutedEventArgs e)
        {
            if(this.txtAdresseIPecombox.Text != "" && this.txtPortECombox.Text != "" && this.txtPortPortainer.Text != "" && this.txtPortSites.Text != ""){
                string user = Environment.GetEnvironmentVariable("USERPROFILE");
                string chemin = string.Format(@"{0}/.docker/confEcombox/param.conf", user);
                List<string> listLines = new List<string>();
                using (StreamReader reader = new StreamReader(chemin))
                {
                    string line;
                    
                    while ((line = reader.ReadLine()) != null)
                    {
                        listLines.Add(line);
                    }


                    if (btnActiveProxy.IsChecked == true && this.txtPortProxy.Text != "" && this.txtipAdresse.Text != "")
                    {              
                        listLines[4] = listLines[4].Split('=')[0]+ "=" + this.txtipAdresse.Text + ":" + this.txtPortProxy.Text;
                        listLines[5] = listLines[5].Split('=')[0] + "=" + this.txtProxyExceptions.Text;       
                    }
                    else
                    {
                        listLines[4] = listLines[4].Split('=')[0] + "=";
                        listLines[5] = listLines[5].Split('=')[0] + "=";
                    }
                    listLines[0] = listLines[0].Split('=')[0] + "=" + this.txtAdresseIPecombox.Text;
                    listLines[1] = listLines[1].Split('=')[0] + "=" + this.txtPortECombox.Text;
                    listLines[2] = listLines[2].Split('=')[0] + "=" + this.txtPortPortainer.Text;
                    listLines[3] = listLines[2].Split('=')[0] + "=" + this.txtPortSites.Text;
                    
                }
                File.WriteAllLines(chemin, listLines);
                MessageBox.Show("Modification enregistré");
            }
        }

        private void btnRestaure_Click(object sender, RoutedEventArgs e)
        {
            addConfiguration();
        }



        public void btnActiveProxy_Checked(object sender, RoutedEventArgs e)
        {
            VerifButon();
        }
    }
}
