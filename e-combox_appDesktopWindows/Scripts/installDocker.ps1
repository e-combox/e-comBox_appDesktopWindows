﻿#Requires -RunAsAdministrator

# Téléchargement de l'exécutable
#(New-Object System.Net.WebClient).DownloadFile('https://download.docker.com/win/stable/Docker%20for%20Windows%20Installer.exe', 'docker-installer.exe')
(New-Object System.Net.WebClient).DownloadFile('https://download.docker.com/win/stable/Docker%20Desktop%20Installer.exe', 'Docker Desktop Installer.exe')

# Installation en mode silentieux avec les options par défaut
#start-process -wait docker-installer.exe " install --quiet"
start-process -wait 'Docker Desktop Installer.exe' " install --quiet"

# Suppression de l'exécutable
#rm docker-installer.exe
rm 'Docker Desktop Installer.exe'

# Ajout du dossier qui va accueillir les logs
If (-not (Test-Path "$env:USERPROFILE\.docker")) { New-Item -ItemType Directory -Path "$env:USERPROFILE\.docker" }
New-Item -Path "$env:USERPROFILE\.docker\logEcombox" -ItemType Directory -force


# Lancement de docker
#$processes = Get-Process "*docker desktop*"
#if ($processes.Count -gt 0)
#{
#    $processes[0].Kill()
#    $processes[0].WaitForExit()
#}
Start-Process "C:\Program Files\Docker\Docker\Docker Desktop.exe"

# Mise du Swap à 4096 Mo
#$content = Get-Content "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" | foreach { $_ -replace "`"swapMiB`": 1024","`"swapMiB`": 4096" } 
#Set-Content -Path "$env:USERPROFILE\AppData\Roaming\Docker\settings.json" -Value $content
