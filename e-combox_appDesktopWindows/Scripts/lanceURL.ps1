﻿# lancement de l'URL avec un éventuel démarrage de Docker

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
#$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "..\..\Scripts\fonctions.ps1"

$ErrorActionPreference = "SilentlyContinue"
#pipeline.Commands.Add("out-default");
#pipeline.Commands.AddScript(@"$ErrorActionPreference = ""SilentlyContinue"");

Write-Output "" >> $pathlog\ecombox.log
Write-Output "=============================================================" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') -  Lancement de l'URL" >> $pathlog\ecombox.log
Write-Output "=============================================================" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

# Vérification que Docker fonctionne correctement sinon on le redémarre
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Vérification de Docker." >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Fin du processus de vérification de Docker." >> $pathlog\ecombox.log

# Lancement de e-comBox dans un navigateur
Write-Output " `t L'application e-comBox va être lancée dans le navigateur." >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log 

#Lancement de l'URL
write-host "Test"
lanceURL

 