﻿# Désinstallation

if (docker ps -q) {     
           
      docker stop $(docker ps -q)          
      }

docker rm -fv $(docker ps -a --format '{{.Names}}' |  Select-String -Pattern e-combox, docker-gen, nginx, portainer, prestashop, woocommerce, humhub, kanboard, mautic, blog, odoo, suitecrm)  *>> $pathlog\ecombox.log
docker volume prune -f
docker image prune -af
docker network prune -f