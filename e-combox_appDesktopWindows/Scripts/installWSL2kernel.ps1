﻿#Requires -RunAsAdministrator

# Installation wsl2-kernel
New-Item -Path "$env:USERPROFILE\.docker\test1Install" -ItemType file -force

#$info = (wsl --set-default-version 2)
#Invoke-Command -ScriptBlock { & wsl --set-default-version 2 }

#if (($info -ilike "*à*") -or ($info -ilike "*error*")) {
$wslUpdateSource = "https://wslstorestorage.blob.core.windows.net/wslblob/wsl_update_x64.msi"   
$wslMsiArgs = @(
    '/i',
    $wslUpdateSource,
    '/quiet',
    '/norestart'
)

Start-Process -FilePath msiexec -ArgumentList $wslMsiArgs -NoNewWindow -Wait

New-Item -Path "$env:USERPROFILE\.docker\test2Install" -ItemType file -force

#}

New-Item -Path "$env:USERPROFILE\.docker\test3Install" -ItemType file -force