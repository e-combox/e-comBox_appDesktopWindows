﻿
# Mise à jour du fichier de paramètres
$ligne = Get-Content $pathconf\param.conf | Select-String ADRESSE_IP
(Get-Content -Path $pathconf\param.conf) | ForEach-Object {$_ -replace "$ligne", "ADRESSE_IP=$docker_ip_host"} | Set-Content -Path $pathconf\param.conf
configPortainer 
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Fin du processus de configuration de Portainer." >> $pathlog\ecombox.log
