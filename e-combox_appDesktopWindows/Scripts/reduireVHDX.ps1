﻿# Optimisation du VHDX

# Déclaration des chemins (logs, scripts et bibliothèque des fonctions)
$pathlog="$env:USERPROFILE\.docker\logEcombox"
$pathconf="$env:USERPROFILE\.docker\confEcombox"
$pathscripts="C:\Program Files\e-comBox\scripts\"
#. "$env:USERPROFILE\e-comBox_setupWin10pro\fonctions.ps1"
. "$pathscripts\fonctions.ps1"

# Suppression des images non utilisées (si script à part)
nettoyageImages


# Optimisation du VHDX
cd $env:LOCALAPPDATA\Docker\wsl\data
wsl --shutdown
optimize-vhd -Path .\ext4.vhdx -Mode full

# Relance de Docker (si script à part)
demarreDocker

