﻿Function valeurConfig
{
    $path = "$env:USERPROFILE\.docker\confEcombox\param.conf"
    If (-not (Test-Path $path))
    {
     curl https://gitlab.com/e-combox/e-comBox_setupWin10pro/raw/dev/param.conf -OutFile $path
    }
    
    $file = new-Object System.IO.StreamReader(Get-item –Path "$path")
    $array = @{}    
    while($line = $file.ReadLine())
    {
        $splitLine = $line.Split('=')
        $id = $splitLine[0]
        $value = $splitLine[1]
        $array.Add($id,$value)
    }    
    $file.Close()
    return $array    
} 

Function ajoutInfosLog
{
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le sytème utilisé" >> $pathlog\ecombox.log
    Get-ComputerInfo | select OSName, OsArchitecture, OsVersion, WindowsVersion, WindowsCurrentVersion, WindowsBuildLabEx, OsHardwareAbstractionLayer, OsTotalVisibleMemorySize, OsFreePhysicalMemory, OsTotalVirtualMemorySize, OsFreeVirtualMemory, OsInUseVirtualMemory, OsTotalSwapSpaceSize | fl  >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "Informations sur le CPU et la RAM utilisé par Docker" >> $pathlog\ecombox.log
    docker info | Select-String "CPU" >> $pathlog\ecombox.log
    docker info 2>$null | Select-String "Memory" >> $pathlog\ecombox.log
}


Function recupIPhost
{
    # Récupération et mise au bon format de l'adresse IP de l'hôte (l'adresse IP récupérée est celle associée à une passerelle par défaut)
    #$docker_ip_host = (Get-NetIPAddress -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPv4Address
    $docker_ip_host = (Get-NetIPAddress -InterfaceIndex (Get-NetIPConfiguration | Foreach IPv4DefaultGateway).ifIndex).IPv4Address  | Select-Object -first 1
    $docker_ip_host = "$docker_ip_host"
    $docker_ip_host = $docker_ip_host.Trim()

    return $docker_ip_host
}


Function recupIPvalides
{
   # Récupération des adresses IP d'une interface physique même si elles ne sont pas associées à une passerelle par défaut
   $adressesIPvalides = (Get-NetIPAddress -InterfaceIndex (Get-NetAdapter -Physical).InterfaceIndex).IPv4Address | Select-String -NotMatch ^169
   $adressesIPvalides = "$adressesIPvalides"
   $adressesIPvalides = $adressesIPvalides.Trim()
 
   return $adressesIPvalides
  
}


Function popupInformation 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 4, $titre, 64) 
   return "$codeBouton"
}


Function popupExclamation 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 48) 
}


Function popupQuestion 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 36) 
}

Function popupQuestionDefautNon 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 292) 
}

Function popupStop 
{ 
   Param([string]$message=’Message...’, [string]$titre=’Titre’) 
 
   $WshShell = New-Object -ComObject wscript.Shell 
   $WshShell.Popup($message, 0, $titre, 20) 
}

Function testConnectInternet
{
   $adressesIPvalides = recupIPvalides
   write-host $adressesIPvalides
 

   if (($adressesIPvalides -eq $null) -or! (Test-Connection gitlab.com -ErrorAction SilentlyContinue)) {
 
      $valideIP = popupExclamation -titre "Configuration de l'adresse IP" -message "Le système ne détecte aucune adresse IP permettant l'accès à Internet nécessaire pour pouvoir utiliser e-comBox. `n`nPour autant l'adresse IP $docker_ip_host figure dans le fichier de paramètres mais l'application ne peut être configuré avec celle-ci. `n`nVérifiez votre configuration IP et relancez le programme." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log  
      Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
      Write-host ""
      exit
    } 
     
}

Function demarreDocker
{

    $WarningPreference='silentlycontinue'

    #Lancement de Docker en super admin
    #if (!([Security.Principal.WindowsPrincipal][Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole([Security.Principal.WindowsBuiltInRole] "Administrator")) { Start-Process powershell.exe "-NoProfile -WindowStyle Normal -ExecutionPolicy Bypass -File `"$PSCommandPath`"" -Verb RunAs; exit }
         Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Arrêt des processus résiduels." >> $pathlog\ecombox.log         

         $process = Get-Process "com.docker.backend" -ErrorAction SilentlyContinue
         if ($process.Count -gt 0)
         {
            Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend existe et va être stoppé" >> $pathlog\ecombox.log
            Stop-Process -Name "com.docker.backend" -Force  >> $pathlog\ecombox.log        
         }
            else {
                Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Le processus com.docker.backend n'existe pas" >> $pathlog\ecombox.log                                 
            }


         $service = get-service com.docker.service
         if ($service.status -eq "Stopped")
         {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Rien à faire car Docker est arrêté." >> $pathlog\ecombox.log             
         }
            else
            {
               Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Le service Docker va être stoppé." >> $pathlog\ecombox.log
               net stop com.docker.service >> $pathlog\ecombox.log              
            }


           foreach($svc in (Get-Service | Where-Object {$_.name -ilike "*docker*" -and $_.Status -ieq "Running"}))
           {
              $svc | Stop-Service -ErrorAction Continue -Confirm:$false -Force
              $svc.WaitForStatus('Stopped','00:00:20')               
           }

           Get-Process | Where-Object {$_.Name -ilike "*docker*"} | Stop-Process -ErrorAction Continue -Confirm:$false -Force            

          foreach($svc in (Get-Service | Where-Object {$_.name -ilike "*docker*" -and $_.Status -ieq "Stopped"} ))
          {
             $svc | Start-Service 
             $svc.WaitForStatus('Running','00:00:20')              
          }

          Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Démarrage de Docker"
          & "C:\Program Files\Docker\Docker\Docker Desktop.exe"
          $startTimeout = [DateTime]::Now.AddSeconds(90)
          $timeoutHit = $true
           
          while ((Get-Date) -le $startTimeout)
          {

          Start-Sleep -Seconds 10                     

          try
         {
            $info = (docker info 2>&1)
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `tDocker info executed. Is Error?: $($info -ilike "*error*"). Result was: $info" >> $pathlog\ecombox.log            
            if ($info -ilike "*error*")
            {
               Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `tDocker info had an error. throwing..." >> $pathlog\ecombox.log
               throw "Error running info command $info"                
            }
            $timeoutHit = $false
            break
         }
         catch 
         {

             if (($_ -ilike "*error during connect*") -or ($_ -ilike "*errors pretty printing info*")  -or ($_ -ilike "*Error running info command*"))
             {
                 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) -`t Docker Desktop n'a pas encore complètement démarré, il faut attendre." >> $pathlog\ecombox.log
                 #Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) -`t Docker Desktop n'a pas encore complètement démarré, il faut attendre."
             }
             else
            {
                #write-host ""
                write-host "Unexpected Error: `n $_" 2>$null
                Write-Output "Unexpected Error: `n $_" 2>$null >> $pathlog\ecombox.log
                return
            }
         }
         $ErrorActionPreference = 'Stop'
     }
     if ($timeoutHit -eq $true)
     {
         throw "Délai d'attente en attente du démarrage de docker"
     }
        
    # Write-host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer."
     Write-host "Started"
     Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - `t Docker a démarré. Le processus peut continuer." >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log 
}


Function verifDocker
{
    # Vérification que Docker fonctionne correctement sinon ce n'est pas la peine de continuer
    Write-host "jfeuhfuefueh"
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification de Docker." >> $pathlog\ecombox.log
    #Write-host ""
    #Write-host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Vérification de l'état de Docker"
    #Write-host ""

    docker-compose version *>> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
    docker info 2>$null *>> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log

    $error.clear()
    $info_docker = (docker info 2>$null)

    if (($info_docker -ilike "*error*") -or ($error -ilike "*error*"))
      {      
         Write-Output "Docker n'est pas démarré. Le processus doit démarrer Docker avant de continuer..." >> $pathlog\ecombox.log 
         Write-Output "" >> $pathlog\ecombox.log
         #Write-host "Le processus doit démarrer Docker avant de continuer..."
         #write-host ""
         demarreDocker    
      }
      else {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Docker est démarré. Le processus peut continuer..." >> $pathlog\ecombox.log 
            Write-Output "" >> $pathlog\ecombox.log
           # Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Docker est démarré. Le processus peut continuer..." 
           # Write-Host ""          
            }

}

Function verifPortainerApresVerifDocker
{
    # Vérification que Portainer fonctionne correctement sinon on redémarre Docker
    if (Test-Path "$env:USERPROFILE\e-comBox_portainer") {

    if ((docker ps -a -f "status=created" | Select-String e-combox) -or (docker ps -a -f "status=created" | Select-String portainer) -or (docker ps -a -f "status=exited" | Select-String e-combox) -or (docker ps -a -f "status=exited" | Select-String portainer)) 
       { 
         Write-Output "Portainer a mal redémarré. Le processus doit re-démarrer Docker avant de continuer..." >> $pathlog\ecombox.log 
         Write-Output "" >> $pathlog\ecombox.log
         Write-host "Le processus doit re-démarrer Docker avant de continuer..."
         write-host ""
         demarreDocker    
       }
       else {
            Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Portainer est correctement lancé. Le processus peut continuer..." >> $pathlog\ecombox.log 
            Write-Output "" >> $pathlog\ecombox.log
            #Write-Host "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Portainer est correctement lancé. Le processus peut continuer..." 
            #Write-Host ""          
            }
     }
      else {
           Write-Output "Dossier Portainer inexistant - Portainer n'est pas encore installé, ceci est normal en phase d'initialisation." >> $pathlog\ecombox.log 
           Write-Output "" >> $pathlog\ecombox.log
           #Write-host "Dossier Portainer inexistant - Portainer n'est pas encore installé, ceci est normal en phase d'initialisation"
           #write-host ""
     }
}

Function wslConfig
{
    # Récupération de la mémoire max du PC
    $memoire_pc = (Get-ComputerInfo).CsPhyicallyInstalledMemory/1MB

    # Récupération de la mémoire max utilisée par Docker/WSL2 via le fichier .wslconfig
    $ligne_mem = Get-Content $env:USERPROFILE\.wslconfig | Select-String memory
    $memoire_wsl2 = ("$ligne_mem".Split('='))[1]
    
    # Configuration éventuelle de la mémoire vive utilisée par WSL2
    $reponse = popupQuestionDefautNon -titre "Configuration de la mémoire vive (RAM) utilisée" -message "Vous disposez au total de $memoire_pc Gigabit et l'e-comBox utilise $memoire_wsl2.`n`nVous pouvez changer ce paramètre sachant que Windows a besoin d'au moins 4GB de libre pour fonctionner correctement.`n`nDésirez-vous modifier la quantité de RAM dédiée à e-comBox ?" -Foreground Yellow 
 
    if ($reponse -eq 6) 
     {
        $RAM=Read-Host "`n`nSaisissez la quantité de mémoire vive (RAM) que vous désirez allouer à l'e-comBox en saisissant également l'unité (par exemple 8GB ou 512MB)."
             
        # Affectation de cette mémoire au fichier .wslconfig
        (Get-Content -Path $env:USERPROFILE\.wslconfig) | ForEach-Object {$_ -replace "$ligne_mem", "memory=$RAM"} | Set-Content -Path $env:USERPROFILE\.wslconfig
                
        # Redémarrage de wsl
        $info = popupInformation -titre "Redémarrage de Docker" -message "Docker va automatiquement redémarrer pour prendre en compte les modifications."
        wsl --shutdown
        demarreDocker
     }
}

Function retourneAdresseProxy {

  $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"

  $settings = Get-ItemProperty -Path $reg

  if ($settings.ProxyEnable -eq 1) {
    $adresseProxy = $settings.ProxyServer
    if ($adresseProxy -ilike "*=*")
        {
            $adresseProxy = $adresseProxy -replace "=","://" -split(';') | Select-Object -First 1
        }

        else
        {
            $adresseProxy = "http://" + $adresseProxy
        }
    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "le proxy est activé et configuré à $adresseProxy" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
       
  }
  else {
    $adresseProxy = ""
  }

  return $adresseProxy
}




Function retourneNoProxy {

  $reg = "HKCU:\Software\Microsoft\Windows\CurrentVersion\Internet Settings"

  $settings = Get-ItemProperty -Path $reg

  if ($settings.ProxyEnable -eq 1) {
    $noProxy = $settings.ProxyOverride

    if ($noProxy)
       { 
             $noProxy = $noProxy.Replace(';',',')
       }
       else
       {     
             $noProxy = "localhost"
       }

    Write-Output "" >> $pathlog\ecombox.log
    Write-Output "le proxy est activé et le noProxy est $noProxy" >> $pathlog\ecombox.log
    Write-Output "" >> $pathlog\ecombox.log
  }
   else {
    $noProxy = ""
  }
  return $noProxy
}



Function configProxyGit 
{ 
 $adresseProxy = retourneAdresseProxy
 $noProxy = retourneNoProxy

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Configuration du Proxy pour GIT" >> $pathlog\ecombox.log 
 Write-Output "" >> $pathlog\ecombox.log


 Set-Location -Path $env:USERPROFILE

 if ($adresseProxy) {      
    # Configuration de Git
    git config --global http.proxy $adresseProxy *>> $pathlog\ecombox.log
    }

    else {
      # Configuration de Git
      git config --global --unset http.proxy *>> $pathlog\ecombox.log
      }
    
}


Function configProxyDocker
{
 $adresseProxy = retourneAdresseProxy
 $noProxy = retourneNoProxy

 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Configuration du proxy pour Docker" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log

 
 Set-Location -Path $env:USERPROFILE\.docker

 if ($adresseProxy) {

     new-item "config.json" –type file -force *>> $pathlog\ecombox.log

     If ($? -eq 0) {
        $allProcesses = Get-Process
        foreach ($process in $allProcesses) { 
             $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\.docker\config.json"} | Stop-Process -Force -ErrorAction SilentlyContinue
        }
        Remove-Item "config.json" *>> $pathlog\ecombox.log
        New-Item -Path "config.json" -ItemType file -force *>> $pathlog\ecombox.log
       }

@"
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "$adresseProxy",
     "httpsProxy": "$adresseProxy",
     "noProxy": "$noProxy"
   }
 }
}
"@ > config.json

     Set-Content config.json -Encoding ASCII -Value (Get-Content config.json) *>> $pathlog\ecombox.log

     Write-Output ""  >> $pathlog\ecombox.log
     Write-Output "Le fichier config.json a été créé et complété."  >> $pathlog\ecombox.log
     Write-Output ""  >> $pathlog\ecombox.log
     }
     else {
         remove-item "config.json" *>> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log
         Write-Output "Le proxy est désactivé et le fichier config.json a été supprimé"  >> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log         
      }
}

Function configProxyDockerAvecPopup
{

 $adresseProxy = retourneAdresseProxy
 $noProxy = retourneNoProxy
 Write-Output "" >> $pathlog\ecombox.log
 Write-Output "Popup pour la vérification de la configuration du Proxy sur Docker" >> $pathlog\ecombox.log
 Write-Output "" >> $pathlog\ecombox.log

 Set-Location -Path $env:USERPROFILE\.docker

 if ($adresseProxy) {
     popupExclamation -titre "Configuration du proxy" -message "Le système a détecté que vous utilisez un proxy pour vous connecter à Internet, vérifiez que ce dernier soit correctement configuré au niveau de Docker avec les paramètres suivants :`n`nAdresse IP du proxy (avec le port utilisé) : $adresseProxy `nBy Pass : $noProxy `n`nSi vous venez de procéder à la configuration, il faut attendre que Docker ait redémarré avant de cliquer sur OK pour continuer."
     #Ajout
     new-item "config.json" –type file -force *>> $pathlog\ecombox.log

     If ($? -eq 0) {
        $allProcesses = Get-Process
        foreach ($process in $allProcesses) { 
             $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\.docker\config.json"} | Stop-Process -Force -ErrorAction SilentlyContinue
        }
        #Remove-Item "config.json" *>> $pathlog\ecombox.log
        New-Item -Path "config.json" -ItemType file -force *>> $pathlog\ecombox.log
       }

@"
{
 "proxies":
 {
   "default":
   {
     "httpProxy": "$adresseProxy",
     "httpsProxy": "$adresseProxy",
     "noProxy": "$noProxy"
   }
 }
}
"@ > config.json

     Set-Content config.json -Encoding ASCII -Value (Get-Content config.json) *>> $pathlog\ecombox.log

     Write-Output ""  >> $pathlog\ecombox.log
     Write-Output "Le fichier config.json a été créé et complété."  >> $pathlog\ecombox.log
     Write-Output ""  >> $pathlog\ecombox.log
     # Fin ajout

}
     else {
         #Ajout
         #remove-item "config.json" *>> $pathlog\ecombox.log
         # Fin ajout
         Write-Output ""  >> $pathlog\ecombox.log
         Write-Output "Le proxy est désactivé et le fichier config.json a été supprimé"  >> $pathlog\ecombox.log
         Write-Output ""  >> $pathlog\ecombox.log
         
         popupExclamation -titre "Configuration du proxy" -message "Le système a détecté que vous n'utilisez pas de proxy pour vous connecter à Internet, vérifiez que cette fonctionnalité soit bien désactivée sur Docker. `n`nSi vous venez de procéder à la désactivation, il faut attendre que Docker ait redémarré avant de cliquer sur OK pour continuer."    
      }

}


Function recupPortainer 
{ 
Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Récupération de portainer" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

$Path="$env:USERPROFILE\e-comBox_portainer\"
$TestPath=Test-Path $Path

If ($TestPath -eq $False) {
    # Récupération de Portainer sur Git
    Write-Output ""  >> $pathlog\ecombox.log
    Write-Output "Téléchargement de Portainer" >> $pathlog\ecombox.log
    write-host ""
    write-host "Téléchargement de Portainer"
    Set-Location -Path $env:USERPROFILE
    git clone -b dev https://gitlab.com/e-combox/e-comBox_portainer.git *>> $pathlog\ecombox.log
    }
    else {
      # Suppression de portainer et installation d'un éventuel nouveau Portainer
      Write-Output ""  >> $pathlog\ecombox.log
      Write-Output "    --> Portainer est installé, il faut le supprimer pour le réinstaller."  >> $pathlog\ecombox.log
      Write-Output ""  >> $pathlog\ecombox.log
      write-host ""
      write-host "Téléchargement de Portainer"
      Set-Location -Path $Path    
      docker-compose down *>> $pathlog\ecombox.log

      Set-Location -Path $env:USERPROFILE   
      Remove-Item "$env:USERPROFILE\e-comBox_portainer" -Recurse -Force *>> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "Téléchargement de Portainer" >> $pathlog\ecombox.log
      git clone -b dev https://gitlab.com/e-combox/e-comBox_portainer.git *>> $pathlog\ecombox.log 
    }
   
If (Test-Path "$env:USERPROFILE\e-comBox_portainer") {
    write-host ""
    write-host "Succès... Portainer a été téléchargé."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Succès... Portainer a été téléchargé." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log

}
    else {
       write-host ""
       write-host "Portainer n'a pas pu être téléchargé. Il y a un problème avec la commande git. Le processus va s'arrêter car ce n'est pas la peine de continuer. Consultez les logs pour avoir les détails de l'erreur."
       Write-Host ""
       write-Output "" >> $pathlog\ecombox.log
       write-Output "Portainer n'a pas pu être téléchargé. Il y a un problème avec la commande git" >> $pathlog\ecombox.log
       write-Output "" >> $pathlog\ecombox.log
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
       exit
    }      
}


Function configPortainer 
{
$config = valeurConfig
Set-Location -Path $env:USERPROFILE\e-comBox_portainer\ *>> $pathlog\ecombox.log

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de Portainer" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de Portainer" 

# Récupération de l'IP du poste
$docker_ip_host = $config.ADRESSE_IP

# Récupération du port (8880 par défaut) dans le fichier paramètre
$portainer_port = $config.PORT_PORTAINER

# Mise à jour de l'adresse IP et du port pour Portainer dans le fichier ".env"
New-Item -Name ".env" -ItemType file -force *>> $pathlog\ecombox.log

If ($? -eq 0) {
  $allProcesses = Get-Process
  foreach ($process in $allProcesses) { 
    $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\e-comBox_portainer\.env"} | Stop-Process -Force -ErrorAction SilentlyContinue
  }

Remove-Item ".env"  *>> $pathlog\ecombox.log
New-Item -Path ".env" -ItemType file -force  *>> $pathlog\ecombox.log
}

Write-Output ""  >> $pathlog\ecombox.log
Write-Output "le fichier .env a été créé"  >> $pathlog\ecombox.log
Write-Output ""  >> $pathlog\ecombox.log

@"
URL_UTILE=$docker_ip_host
PORT=$portainer_port
"@ > .env

Set-Content .env -Encoding ASCII -Value (Get-Content .env)
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été mis à jour avec l'adresse IP $docker_ip_host et le port $portainer_port"  >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

}


Function configPortainerAvecPopup 
{
#$config = valeurConfig
}

Function startPortainer 
{ 
# Lancement de Portainer - URL : http://localhost:8880/portainer)
Set-Location -Path $env:USERPROFILE\e-comBox_portainer\

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement de Portainer" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement de Portainer" 

If (docker ps -a | Select-String "name=portainer-app") {
   docker rm -f portainer-app *>> $pathlog\ecombox.log
   }
If (docker ps -a | Select-String "name=portainer-proxy") {
   docker rm -f portainer-proxy *>> $pathlog\ecombox.log
   }
If (docker ps -a --format '{{.Names}}' |  Select-String portainer-proxy) {
   docker rm -f $(docker ps -a --format '{{.Names}}' |  Select-String portainer-proxy)  *>> $pathlog\ecombox.log
   }
docker-compose up -d *>> $pathlog\ecombox.log

}


Function verifPortainer {
if ((docker ps |  Select-String portainer-proxy)) {
    write-host ""
    write-host "Portainer est UP, on peut continuer."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Portainer est UP, on peut continuer" >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log
    }
      else {
            write-host ""
            write-host "Toutes les tentatives pour démarrer Portainer ont échoué, essayez de stopper et de démarrer e-comBox puis de réinitialiser l'environnement"
            Write-Host ""
            Write-Output "" >> $pathlog\ecombox.log
            Write-Output "Toutes les tentatives pour démarrer Portainer ont échoué" >> $pathlog\ecombox.log
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
            exit
            }    
}

Function recupReverseProxy 
{ 

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$((Get-date -format 'dd/MM/yy_HH:mm:ss')) - Récupération du reverse proxy" >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

$Path="$env:USERPROFILE\e-comBox_reverseproxy\"
$TestPath=Test-Path $Path

If ($TestPath -eq $False) {
    # Récupération du reverse proxy sur Git
    Write-Output ""  >> $pathlog\ecombox.log
    Write-Output "Téléchargement du reverse proxy" >> $pathlog\ecombox.log
    write-host ""
    write-host "Téléchargement du reverse proxy"
    Set-Location -Path $env:USERPROFILE
    git clone -b master https://gitlab.com/e-combox/e-comBox_reverseproxy.git *>> $pathlog\ecombox.log
    }
    else {
      # Suppression du reverse proxy et installation d'un éventuel nouveau reverse proxy
      Write-Output ""  >> $pathlog\ecombox.log
      Write-Output "    --> Le reverse proxy existe et va être remplacé."  >> $pathlog\ecombox.log
      Write-Output ""  >> $pathlog\ecombox.log
      write-host ""

      Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy\ *>> $pathlog\ecombox.log 
      docker-compose down *>> $pathlog\ecombox.log
      docker image rm -f reseaucerta/docker-gen:2.1 *>> $pathlog\ecombox.log
      docker volume rm e-combox_reverseproxy_nginx-html *>> $pathlog\ecombox.log
      docker volume rm e-combox_reverseproxy_nginx-docker-gen-templates *>> $pathlog\ecombox.log

      Set-Location -Path $env:USERPROFILE   
      Remove-Item "$env:USERPROFILE\e-comBox_reverseproxy" -Recurse -Force *>> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "Téléchargement du reverse proxy" >> $pathlog\ecombox.log
      write-host "Téléchargement du reverse proxy"  
      git clone -b master https://gitlab.com/e-combox/e-comBox_reverseproxy.git *>> $pathlog\ecombox.log 
    }
   
If (Test-Path "$env:USERPROFILE\e-comBox_reverseproxy") {
    write-host ""
    write-host "Succès... Le reverse proxy a été téléchargé."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Succès... Le reverse proxy a été téléchargé." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log

}
    else {
       write-host ""
       write-host "Le reverse proxy n'a pas pu être téléchargé. Il y a un problème avec la commande git. Le processus va s'arrêter car ce n'est pas la peine de continuer. Consultez les logs pour avoir les détails de l'erreur."
       Write-Host ""
       write-Output "" >> $pathlog\ecombox.log
       write-Output "Le reverse proxy n'a pas pu être téléchargé. Il y a un problème avec la commande git" >> $pathlog\ecombox.log
       write-Output "" >> $pathlog\ecombox.log
       # Ce n'est pas la peine de continuer
       Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
       Write-host ""
       exit
    }  
}

Function configReverseProxy 
{
$config = valeurConfig
Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy\ *>> $pathlog\ecombox.log

# Récupération de l'IP du poste dans le fichier paramètre
$docker_ip_host = $config.ADRESSE_IP

# Récupération du port (8888 par défaut) dans le fichier paramètre
$rp_port = $config.PORT_RP

# Mise à jour de l'adresse IP et du port pour le reverse proxy dans le fichier ".env"
New-Item -Name ".env" -ItemType file -force *>> $pathlog\ecombox.log

If ($? -eq 0) {
  $allProcesses = Get-Process
  foreach ($process in $allProcesses) { 
    $process.Modules | where {$_.FileName -eq "$env:USERPROFILE\e-comBox_reverseproxy\.env"} | Stop-Process -Force -ErrorAction SilentlyContinue
  }

Remove-Item ".env"  *>> $pathlog\ecombox.log
New-Item -Path ".env" -ItemType file -force  *>> $pathlog\ecombox.log
}

Write-Output ""  >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été créé."  >> $pathlog\ecombox.log
Write-Output ""  >> $pathlog\ecombox.log

@"
URL_UTILE=$docker_ip_host
NGINX_PORT=$rp_port
"@ > .env

Set-Content .env -Encoding ASCII -Value (Get-Content .env)
Write-Output "" >> $pathlog\ecombox.log
Write-Output "Le fichier .env a été mis à jour avec l'adresse IP $docker_ip_host et le port $rp_port"  >> $pathlog\ecombox.log
Write-Output "" >> $pathlog\ecombox.log

}

Function configReverseProxyAvecPopup 
{
$config = valeurConfig
}

Function startReverseProxy 
{ 
# Lancement du Reverse Proxy

Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy\ *>> $pathlog\ecombox.log

Write-Output "" >> $pathlog\ecombox.log
Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement du Reverse Proxy" >> $pathlog\ecombox.log
Write-Host ""
Write-Host "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Lancement du Reverse Proxy" 

Set-Location -Path $env:USERPROFILE\e-comBox_reverseproxy\ *>> $pathlog\ecombox.log
docker-compose up -d *>> $pathlog\ecombox.log

}


Function verifReverseProxy {
if ((docker ps |  Select-String nginx)) {
    write-host ""
    write-host "Le Reverse Proxy est UP, on peut continuer."
    write-host ""
    write-Output "" >> $pathlog\ecombox.log
    write-Output "Le Reverse Proxy est UP, on peut continuer." >> $pathlog\ecombox.log
    write-Output "" >> $pathlog\ecombox.log
    }
      else {
            write-host ""
            write-host "Toutes les tentatives pour démarrer Reverse Proxy ont échoué, essayez de stopper et de démarrer e-comBox puis de réinitialiser l'environnement"
            Write-Host ""
            Write-Output "" >> $pathlog\ecombox.log
            Write-Output "Toutes les tentatives pour démarrer Reverse Proxy ont échoué" >> $pathlog\ecombox.log
            # Ce n'est pas la peine de continuer
            Read-Host "Appuyez sur la touche Entrée pour arrêter le processus"
            Write-host ""
            exit
            }    
}

Function deleteApplication
{
 Write-Output "" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de l'application" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "" >> $pathlog\ecombox.log
 
   #Suppression d'une éventuelle application
   if ((docker ps -a |  Select-String e-combox)) {
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "Suppression d'e-combox avec son volume :" >> $pathlog\ecombox.log
     docker rm -f e-combox *>> $pathlog\ecombox.log
     docker volume rm ecombox_data *>> $pathlog\ecombox.log     
   }
    else {
       Write-Output "" >> $pathlog\ecombox.log
       Write-Output "Pas d'application e-combox trouvée." >> $pathlog\ecombox.log      
     } 
}

Function startApplication
{
  $config = valeurConfig
  $port_externe_ecb = $config.PORT_ECB
  $ports_ecb = -join("$port_externe_ecb",":","80")
  
  Write-Output "" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Téléchargement et lancement de l'application" >> $pathlog\ecombox.log
  Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
  Write-Output "" >> $pathlog\ecombox.log

  # Téléchargement d'e-comBox
  Write-Output "" >> $pathlog\ecombox.log
  Write-Output "Téléchargement et lancement d'e-combox :" >> $pathlog\ecombox.log      
  docker pull reseaucerta/e-combox:dev *>> $pathlog\ecombox.log

  # Lancement d'e-comBox
  docker run -dit --name e-combox -v ecombox_data:/usr/local/apache2/htdocs/ -v ecombox_config:/etc/ecombox-conf --restart always -p $ports_ecb --network bridge_e-combox reseaucerta/e-combox:dev *>> $pathlog\ecombox.log
    
  Write-host "Téléchargement et lancement du conteneur FAIT"
}

Function configApplication
{
     Write-Output "" >> $pathlog\ecombox.log
     Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
     Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Configuration de l'application" >> $pathlog\ecombox.log
     Write-Output "=======================================================================================================" >> $pathlog\ecombox.log
     Write-Output "" >> $pathlog\ecombox.log

     # Récupération de l'URL d'accès à portainer
     $ancienneURL =  docker exec e-combox bash -c 'grep -ni \"var endpoint\" htdocs/main.js | cut -d\"/\" -f3'

     # Remplacement par la bonne URL
     $env = Get-Content $env:USERPROFILE\e-comBox_portainer\.env
     $ip_host = $env[0].split("=")[1]
     $port_portainer = $env[1].split("=")[1]
     
     $nouvelleURL = -join("$ip_host",":","$port_portainer")

     if ($ancienneURL -ne $nouvelleURL) {
        #Write-Host "L'ancienne URL $ancienneURL sera remplacée par la nouvelle $nouvelleURL."
        Write-Output "" >> $pathlog\ecombox.log
        Write-Output "L'ancienne URL $ancienneURL sera remplacée par la nouvelle $nouvelleURL." >> $pathlog\ecombox.log
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/main.js.map
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/app-ecombox-ecombox-module.js
        docker exec e-combox sed -i "s/$ancienneURL/$nouvelleURL/g" htdocs/app-ecombox-ecombox-module.js.map
        }
        else {
           #Write-Host "L'ancienne URL $ancienneURL est identique à la nouvelle $nouvelleURL."
           Write-Output "" >> $pathlog\ecombox.log
           Write-Output "L'ancienne URL $ancienneURL est identique à la nouvelle $nouvelleURL." >> $pathlog\ecombox.log
           }      

}

Function lanceURL
{
      $config = valeurConfig

      # Récupération du l'adresse IP dans le fichier de paramètre      
      $ip_host = $config.ADRESSE_IP
      
      # Récupération du port (8888 par défaut) dans le fichier paramètre
      $ecb_port = $config.PORT_ECB

      $url_acces_ecb = -join("$ip_host",":","$ecb_port")

      Start-Process "http://$url_acces_ecb/" *>> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - L'application e-comBox a été lancée dans le navigateur." >> $pathlog\ecombox.log
      Write-Output "" >> $pathlog\ecombox.log
      popupInformation -titre "Lancement d'e-combox" -message "L'application e-comBox a été lancée dans le navigateur par défaut."
}

Function nettoyageImages
{
      Write-host ""      
      Write-host "Suppression des images si elles ne sont associées à aucun site"
      Write-host ""
      write-Output "" >> $pathlog\ecombox.log
      Write-Output "Suppression des images si elles ne sont associées à aucun site" >> $pathlog\ecombox.log
      docker rmi $(docker images -q) 2>$null *>> $pathlog\ecombox.log

      # Suppression des éventuelles images dangling
      if (docker images -q -f dangling=true) {
          docker rmi $(docker images -q -f dangling=true) 2>$null *>> $pathlog\ecombox.log
      }
}


Function creerPass
{

Param (
  [string]$type)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }
	
if ($type -eq "ecb") {
   $Cred = Get-Credential -Message "Saisissez le nom d'utilisateur et le mot de passe que vous voulez utiliser pour accéder à l'interface d'e-comBox"
   }
   else {
        $Cred = Get-Credential -UserName "admin" -Message "Saisissez le mot de passe que vous avez attribué au compte `"admin`" de Portainer"
      }

if ($cred) {
   
   $password = $cred.GetNetworkCredential().Password
   if ($password -eq "") {
      if ($type -eq "ecb") {
          $retour = popupExclamation -titre "Mot de passe non valide" -message "Le mot de passe est vide. L'authentification à l'e-comBox se fera sans mot de passe."      
          }
          else {
             $retour = popupExclamation -titre "Mot de passe non valide" -message "Le mot de passe est vide. La synchonisation du mot de passe de Portainer ne peut pas s'effectuer. `n`nVeuillez recommencer l'opération."      
             }
      If (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName"
          }
      If (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass"
          }
      }
      else {
        $Cred.UserName | Out-File $pathconf\$fileName
        $Cred.Password | ConvertFrom-SecureString | Out-File $pathconf\$filePass
        if ($type -eq "ecb") {
          $retour = popupInformation -titre "Authentification valide" -message "Le nom d'utilisateur et le mot de passe ont été créés. `n`Merci de vider le cache de votre navigateur afin que la nouvelle authentification à l'e-comBox soit prise en compte."      
          }
          #else {
             #$retour = popupExclamation -titre "Confirmation de l'authentification" -message "La synchonisation du mot de passe de Portainer va s'effectuer."      
             #}    
        Write-Output "Le nom d'utilisateur et le mot de passe ont été créés dans les fichiers $fileName et $filePass."  >> $pathlog\ecombox.log
        Write-Output ""  >> $pathlog\ecombox.log              
        }
    }
    else {
        Write-Output "L'action a été annulée"  >> $pathlog\ecombox.log
        Write-Output ""  >> $pathlog\ecombox.log
    }
}

Function creerAuth
{
If ((Test-Path "$pathconf\authName.txt") -and (Test-Path "$pathconf\authPass.txt")) {

   $name = Get-Content $pathconf\authName.txt
   $securePass = Get-Content $pathconf\authPass.txt | ConvertTo-SecureString
   $auth = New-Object System.Management.Automation.PSCredential -ArgumentList $name,$securePass
   $pass = $auth.GetNetworkCredential().Password

   # Création du .htaccess

   $test_htaccess = docker exec e-combox bash -c 'cat htdocs/.htaccess' 2>&1
      if ($test_htaccess -eq $null)
      {
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
      }
   
   docker exec e-combox bash -c 'touch htdocs/.htaccess' *>> $pathlog\ecombox.log       
   docker exec e-combox bash -c 'echo AuthType Basic > htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthName \"E-COMBOX\" >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthBasicProvider file >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e AuthUserFile /etc/ecombox-conf/.auth >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   docker exec e-combox bash -c 'echo -e Require valid-user >> htdocs/.htaccess' *>> $pathlog\ecombox.log
   # docker exec e-combox bash -c 'cat htdocs/.htaccess'

   $test_auth = docker exec e-combox bash -c 'cat /etc/ecombox-conf/.auth'
   if ($test_auth -eq "null")
      {
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
      }
   
   docker exec e-combox htpasswd -cBb /etc/ecombox-conf/.auth $name $pass *>> $pathlog\ecombox.log
   }
}

Function supprimerAuth {

Param (
  [string]$type
)

if ($type -eq "ecb") {
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }

# Pour l'instant pas de test sur le type car seul le type "ecb" est concerné
If ((Test-Path "$pathconf\$fileName") -or (Test-Path "$pathconf\$filePass"))  {
       if (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName" -Force
          Write-Output "Le fichier $pathconf\$fileName a été supprimé."  >> $pathlog\ecombox.log
          Write-Output ""  >> $pathlog\ecombox.log
          write-host Le fichier $pathconf\$fileName a été supprimé.
          }
       if (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass" -Force
          Write-Output "Le fichier $pathconf\$fileName a été supprimé."  >> $pathlog\ecombox.log
          Write-Output ""  >> $pathlog\ecombox.log
          write-host Le fichier $pathconf\$filePass a été supprimé.
          }
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
       Write-Output "Les fichiers .htaccess et .auth ont été supprimés."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
     }
}      



Function supprimerAuthAvecPopup {

Param (
  [string]$type
)

if ($type -eq "ecb") {
Write-host type est $type
   $fileName = "authName.txt" 
   $filePass = "authPass.txt"
   }
   else {
      $fileName = "portainerName.txt" 
      $filePass = "portainerPass.txt"
      }

# Pour l'instant pas de test sur le type car seul le type "ecb" est concerné
If ((Test-Path "$pathconf\$fileName") -or (Test-Path "$pathconf\$filePass"))  {
    $retour = popupStop -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "Êtes-vous certain de vouloir supprimer l'authentification pour accéder à l'e-comBox ?"      
     
    if ($retour -eq 6) 
       {
       if (Test-Path "$pathconf\$fileName") { 
          Remove-Item "$pathconf\$fileName" -Force
          }
       if (Test-Path "$pathconf\$filePass") { 
          Remove-Item "$pathconf\$filePass" -Force
          }
       docker exec e-combox bash -c 'rm htdocs/.htaccess' *>> $pathlog\ecombox.log
       docker exec e-combox bash -c 'rm /etc/ecombox-conf/.auth' *>> $pathlog\ecombox.log
     
       $retour = popupExclamation -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "L'accès à l'e-comBox se fait maintenant sans authentification. `n`Si vous rencontrez un problème, videz le cache de votre navigateur."
       Write-Output "Les fichiers $pathconf\$fileName et $pathconf\$filePass contenant les nom d'utilisateur et le mot de passe ont été supprimés"  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
       Write-Output "Les fichiers .htaccess et .auth ont été supprimés."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log 
       }
       else {
           Write-Output "L'utilisateur a renoncé à la suppression de l'authentification."  >> $pathlog\ecombox.log
           Write-Output ""  >> $pathlog\ecombox.log
          }
    }
    else {
       $retour = popupExclamation -titre "Suppression de l'authentification pour accéder à l'e-comBox" -message "Aucune authentification pour accéder à l'interface de l'e-comBox n'est actuellement configurée."      
       Write-Output "Aucune authentification configurée : pas de proposition de suppression."  >> $pathlog\ecombox.log
       Write-Output ""  >> $pathlog\ecombox.log
    }
}      

Function synchroPassPortainer
{
If ((Test-Path "$pathconf\portainerName.txt") -and (Test-Path "$pathconf\portainerPass.txt")) {
   $nom = Get-Content $pathconf\portainerName.txt
   $securePass = Get-Content $pathconf\portainerPass.txt | ConvertTo-SecureString
   $auth = New-Object System.Management.Automation.PSCredential -ArgumentList $nom,$securePass
   $pass = $auth.GetNetworkCredential().Password

   $ancienPass = docker exec e-combox cat htdocs/main.js | Select-String "Password:"
   $ancienPass = $ancienPass -replace ",",""

   docker exec e-combox sed -i "s/$ancienPass/Password: '$pass'/g" htdocs/main.js
   docker exec e-combox sed -i "s/$ancienPass/Password: '$pass'/g" htdocs/main.js.map
   }
}

Function supprimerSites
{

$reponse = popupStop -titre "Suppression des sites" -message "il va être procédé à la suppression de vos sites, les données ne pourront pas être récupérées. `n`nLe mot de passe de Portainer va être réinitialisé et l'éventuelle authentification à l'interface va être supprimée. `n`nLe processus peut être assez long, veuillez patienter... `n`nCliquez sur Oui pour confirmer ou sur Non pour arrêter le processus."

   if ($reponse -eq 6) 
     {
      
      #docker system prune -a --volumes -f --filter "images" *>> $pathlog\ecombox.log
           
      docker rm -fv $(docker ps -a --format '{{.Names}}' |  Select-String -Pattern prestashop, woocommerce, humhub, kanboard, mautic, blog, odoo, suitecrm)  *>> $pathlog\ecombox.log
      docker image prune -af *>> $pathlog\ecombox.log
      

      Write-Output "" >> $pathlog\ecombox.log
      Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de tous les objets réalisée :" >> $pathlog\ecombox.log
      Write-host ""
      Write-Host "La suppression des sites est terminée, le système va procéder à la réinitialisation de l'application e-comBox."      

           
     }   
     else {
          #return $supprimeSite = $false
          Write-Output "" >> $pathlog\ecombox.log
          Write-Output "$(Get-date -format 'dd/MM/yy_HH:mm:ss') - Suppression de tous les objets annulée." >> $pathlog\ecombox.log
          Write-host ""
          Write-Host "La suppression des sites a été annulée, ce n'est pas la peine de réinitialiser l'application."  
          }              
}
