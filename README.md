## e-comBox_appDesktopWindows
Application de bureau pour la gestion d’e-comBox sous Windows 10 Pro

## Objectif E-comBox
E-comBox a pour but de pouvoir fournir aux professeurs un outil leur permettant de distribuer à leurs élèves des sites
web se basant sur des modèles déjà configurés antérieurement, ainsi les élèves n'auront plus à créer leurs sites de A à Z et pourront
le modifier plus facilement et les professeurs auront un contrôle total sur ceux-ci.

## Menu Docker
1.Stopper Docker  
2.Relancer Docker  

## Menu Aide
Le menu aide contient :  
1.Un lien vers la documentation utilisateur.  
2.Un lien vers la documentation administrateur.  
3.Un formulaire afin de remplir un ticket qui sera envoyé au technicien en cas de problème.

## Menu Administration
Le menu administration contient:  
1.Un bouton pour désinstaller e-comBox.    
2.Un bouton pour mettre à jour e-comBox.  
3.Un bouton pour vérifier et configurer e-comBox.  
